function! DetectCfn()
    if (getline(1) =~ "{") && ((getline(1) =~ "AWSTemplateFormatVersion") || (getline(2) =~ "AWSTemplateFormatVersion"))
        set filetype=cloudformation.json
        set syntax=cloudformation-json
    elseif (getline(1) =~ "AWSTemplateFormatVersion:") || (getline(2) =~ "AWSTemplateFormatVersion:")
        set filetype=cloudformation.yaml
        set syntax=cloudformation-yaml
    endif
endfunction

augroup filetypedetect
  au BufRead,BufNewFile * call DetectCfn()
augroup END
