runtime! syntax/yaml.vim

syn cluster yamlFlow      add=yamlTagPrefix

syntax match _Functions     'Fn::\a*:\|Ref:'

hi def link _Functions              Type
hi! def link yamlTagHandle          Type
hi! def link yamlTagPrefix          Type

let b:current_syntax = "cloudformation-yaml"
