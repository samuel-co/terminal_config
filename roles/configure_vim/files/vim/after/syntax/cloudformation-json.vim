runtime! syntax/json.vim

syntax match _Brackets      "[\]\[{}()]"
syntax match _Functions     '"Fn::\a*"\|"Ref"'

hi def link _Brackets       Delimiter
hi def link _Functions      Type

let b:current_syntax = "cloudformation-json"
